DELIMITER //
CREATE PROCEDURE DobaviPodatkeZaGraf (
    IN oznaka_delatnosti CHAR(2)
    IN oznaka_mesta_sedista INT
)
BEGIN
    SELECT poslovni_sistem.oznaka, poslovni_sistem.naziv, grupa_delatnosti.oznaka, grupa_delatnosti.naziv, naseljeno_mesto.naselje, naseljeno_mesto.naziv_naselja
    FROM poslovni_sistem
    INNER JOIN grupa_delatnosti ON poslovni_sistem.grupa_delatnosti grupa_delatnosti.oznaka
    INNER JOIN naseljeno_mesto ON poslovni_sistem.sediste = naseljeno_mesto.naselje
    WHERE poslovni_sistem.grupa_delatnosti = oznaka_delatnosti AND poslovni_sistem.sediste = oznaka_mesta_sedista
END //

DELIMITER ;

CALL DobaviPodatkeZaGraf(oznaka_delatnosti, oznaka_mesta_sedista)