from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config


def call_dobavi_podatke_za_graf(oznaka_delatnosti, oznaka_mesta_sedista):
  try:
    db_config = read_db_config()
    conn = MySQLConnection(**db_config)
    cursor = conn.cursor()
    args = [oznaka_delatnosti, oznaka_mesta_sedista]
    cursor.callproc("DobaviPodatkezaGraf", args)

    for result in cursor.stored_results():
      print(result.fetchall())

  except Error as e:
    print(e)

  finally:
    cursor.close()
    conn.close()